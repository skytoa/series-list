exports.get = function (serie) {
    return (
        '<h2>Ajouter une Série DpStream</h2>' +
        '<div class="form-group">\n' +
        '  <label for="inputAddress">Nom</label>\n' +
        '  <input type="text" class="form-control" id="input-name" value="'+ serie.name +'">\n' +
        '</div>\n' +
        '<div class="form-group">\n' +
        '  <label for="inputAddress2">URL</label>\n' +
        '  <input type="text" class="form-control" id="input-url" value="'+ serie.url +'">\n' +
        '</div>\n' +
        '<div class="form-row">\n' +
        '  <div class="form-group col-md-6">\n' +
        '    <label for="inputCity">Saison</label>\n' +
        '    <input type="number" class="form-control" id="input-season" value="'+ serie.lastEpisode.season +'">\n' +
        '  </div>\n' +
        '  <div class="form-group col-md-6">\n' +
        '    <label for="inputState">Episode</label>\n' +
        '    <input type="number" class="form-control" id="input-episode" value="'+ serie.lastEpisode.episode +'">\n' +
        '  </div>\n' +
        '</div>\n' +
        '<button type="submit" class="btn btn-warning" id="input-submit">Modifer</button>\n'
    );
};