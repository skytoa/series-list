exports.get = function () {
    return (
        '<h2>Ajouter une Série DpStream</h2>' +
        '<div class="form-group">\n' +
        '  <label for="inputAddress">Nom</label>\n' +
        '  <input type="text" class="form-control" id="input-name" placeholder="Ex: Marvel - Agent of Chield">\n' +
        '</div>\n' +
        '<div class="form-group">\n' +
        '  <label for="inputAddress2">URL</label>\n' +
        '  <input type="text" class="form-control" id="input-url" placeholder="Ex: https://www.dpstream.net/serie-5627-marvel-les-agents-du-shield-marvel-s-agents-of-s-h-i-e-l-d.html">\n' +
        '</div>\n' +
        '<div class="form-row">\n' +
        '  <div class="form-group col-md-6">\n' +
        '    <label for="inputCity">Saison</label>\n' +
        '    <input type="number" class="form-control" id="input-season" value="0">\n' +
        '  </div>\n' +
        '  <div class="form-group col-md-6">\n' +
        '    <label for="inputState">Episode</label>\n' +
        '    <input type="number" class="form-control" id="input-episode" value="0">\n' +
        '</div>\n' +
        '<button type="submit" class="btn btn-success" id="input-submit">Ajouter</button>\n'
    );
};