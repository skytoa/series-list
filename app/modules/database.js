let sql = require('sql.js');
let fs = require('fs');

let fileName = "./data.sqlite";
let db = null;

/**
 * Crée une nouvelle base de donnée
 * @returns {database} Base de donée complétement vide
 */
newDatabase = function () {
    return new sql.Database();
};

/**
 * Retourne la base de donnée de l'application
 * @returns {database} La base de donnée de l'application
 */
exports.get = function(){
    if(db !== null) return db;

    if(! fs.existsSync(fileName)) db = newDatabase();
    else db = new sql.Database(fs.readFileSync(fileName));
    return db;
};

/**
 * Sauvegarde la base de donnée de l'application
 * @param database La base de donnée de l'application
 */
exports.save = function(database){
    db = database;
    let buffer = new Buffer(database.export());
    fs.writeFileSync(fileName, buffer);
};

