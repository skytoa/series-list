let list = require('./components/list/list.js');
let add = require('./components/add/add.js');
let change = require('./components/change/change.js');

let components = {
    'list': list,
    'add': add,
    'change': change,
};

let switchScene = function (scene, params) {
    document.body.querySelector("main").innerHTML = "";

    if(params === null){
        components[scene]();
    }else{
        components[scene](params);
    }
};

document.body.querySelector("#BtnList").onclick = function () {
    switchScene("list");
};
document.body.querySelector("#BtnAdd").onclick = function () {
    switchScene("add");
};


module.exports = switchScene;
