let html = require('./html.js');
let Serie = require('../../modules/Serie.js');

module.exports = function () {
    document.body.querySelector("main").innerHTML = html.get();

    document.body.querySelector("main").querySelector("#input-submit").onclick = () => {
        let name = document.querySelector("#input-name").value;
        let url = document.querySelector("#input-url").value;
        let season = document.querySelector("#input-season").value;
        let episode = document.querySelector("#input-episode").value;
        let site = document.querySelector("#input-site").value;

        serie = new Serie(-1, name, url, {season: season, episode:episode});
        serie.addFromDatabase();
        require('../../app.js')("list");
    };
};