let databaseManager = require('./database.js');
let Serie = require('./Serie.js');

/**
 * Retourne la liste de toutes les séries dans la base de donnée
 * @return Array of Serie : Liste contenant les séries.
 */
exports.getSeries = function () {
    let db = databaseManager.get();

    let querry = "SELECT * FROM series;";
    let result = db.exec(querry)[0];

    let series = [];
    result.values.forEach((serieDB) => {
        series[serieDB[0]] = new Serie(serieDB[0], serieDB[1], serieDB[2], {season: serieDB[3], episode: serieDB[4]});
    });
    return series;
};