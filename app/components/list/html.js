exports.baseHtml = function () {
    return '<div class="table-responsive">\n' +
           '  <table class="table table-striped">\n' +
           '    <thead>\n' +
           '      <tr>\n' +
           '        <th>Nom</th>\n' +
           '        <th>Dernier Vu</th>\n' +
           '        <th>Statut</th>\n' +
           '        <th>Actions</th>\n' +
           '      </tr>\n' +
           '    </thead>\n' +
           '    <tbody class="list-series">\n' +
           '    </tbody>\n' +
           '  </table>\n' +
           '</div>';
};

exports.serieHtml = function (serie) {
    return '<tr class="serie" serie="'+ serie.id +'" id="serie-'+ serie.id +'">\n' +
           '  <td>'+ serie.name +'</td>' +
           '  <td class="last-episode">Saison '+ serie.lastEpisode.season +' Episode '+ serie.lastEpisode.episode +'</td>' +
           '  <td>' +
           '    <img src="assets/waiting.gif" alt="waiting"> ' +
           '    <span class="badge badge-pill badge-danger" hidden>New</span> ' +
           '  </td>' +
           '  <td>' +
           '    <button type="button" class="btn btn-primary btn-sm btn-look">Regarder</button> ' +
           '    <button type="button" class="btn btn-success btn-sm btn-looked">Vu</button> ' +
           '    <button type="button" class="btn btn-warning btn-sm btn-change">Modifier</button> ' +
           '    <button type="button" class="btn btn-danger btn-sm btn-delete">Supprimer</button>' +
           '  </td>\n' +
           '</tr>\n';
};