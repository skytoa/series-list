let html = require('./html.js');
let Serie = require('../../modules/Serie.js');

module.exports = function (serie) {
    document.body.querySelector("main").innerHTML = html.get(serie);

    document.body.querySelector("main").querySelector("#input-submit").onclick = () => {
        serie.name = document.querySelector("#input-name").value;
        serie.url = document.querySelector("#input-url").value;
        serie.lastEpisode.season = document.querySelector("#input-season").value;
        serie.lastEpisode.episode = document.querySelector("#input-episode").value;

        serie.updateFromDatabase();
        require('../../app.js')("list");
    };
};