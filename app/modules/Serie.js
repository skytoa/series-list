let databaseManager = require('./database.js');

module.exports = class Serie {

    /**
     * Création d'une instance de Serie.
     * @param id - int : Id de la base de donnée.
     * @param name - string : Nom de la série.
     * @param url - string : URL dpstream de la série.
     * @param lastEpisode - object : Dernière épisode visionné. = {season, episode}
     */
    constructor (id, name, url, lastEpisode) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.lastEpisode = lastEpisode;
    }

    /**
     * Regarde si il un nouvel episode est disponible.
     * @param callbackSuccess - function : Fonction si il y a un nouvelle épisode de disponible. Prend en paramètre un string qui est affiché dans le drappeau
     * @param callbackFail - function : Fonction si aucun épisode n'est disponible.
     */
    newEpisodeAvailable (startSearch, callbackSuccess, callbackFail) {
        startSearch();
        this.checkEpisode(this.getNewEpisodeNumber(), ()=>{callbackSuccess("New Episode"); this.newEpisode={season: this.lastEpisode.season, episode: this.lastEpisode.episode+1}}, ()=>{
            this.checkEpisode(this.getNewSeasonNumber(), ()=>{callbackSuccess("New Saison"); this.newEpisode={season: this.lastEpisode.season+1, episode: 1}}, () => {
                this.newEpisode = null;
                callbackFail();
            });
        });
    }

    /**
     * Ajoute une série à la base de donnée
     */
    addFromDatabase () {
        if(this.id !== -1) { console.log("Déjà dans la base de donnée"); return; }

        let db = databaseManager.get();

        let querry = 'INSERT INTO series VALUES (null, "'+this.name+'", "'+this.url+'", '+this.lastEpisode.season+', '+this.lastEpisode.episode+');';
        db.run(querry);
        databaseManager.save(db);
    }

    /**
     * Met à jour la série dans la base de donnée
     */
    updateFromDatabase () {
        if (this.id === -1) { console.log("Pas encore dans la base de donnée"); return; }

        let db = databaseManager.get();
        let querry = 'UPDATE series ' +
            'SET name="'+ this.name +'", url="'+ this.url +'", season="'+ this.lastEpisode.season +'", episode="'+ this.lastEpisode.episode+'" ' +
            'WHERE id="'+ this.id + '";';
        db.run(querry);
        databaseManager.save(db);
    }

    /**
     * Supprime la série de la base de donnée
     */
    deleteFromDatabase () {
        let db = databaseManager.get();

        let querry = "DELETE FROM series WHERE id="+ this.id +";";
        db.run(querry);
        databaseManager.save(db);
    }

    /**
     * Fait la requette HTTPS sur le site DpStream pour regarder si l'épisode passé en paramètre est disponible
     * @param episodeNumber - string : Numéro de l'épisode dont on regarde si il est disponible
     * @param callbackSuccess - function : Fonction exécuté si l'épisode est disponible
     * @param callbackFail - function : Fonction exécuté si l'épisode n'est pas disponible
     */
    checkEpisode (episodeNumber, callbackSuccess, callbackFail) {
        let find = false;
        require('https').get(this.url, function (res) {
            let buff = new Buffer(0);

            res.on('data', function (data) {
                let line = data.toString('utf8');
                if(!find && line.includes(episodeNumber)) {
                    find = true;
                    callbackSuccess();
                    return true;
                }
            });

            res.on('end', function () {
                if(!find){
                    callbackFail();
                    return false;
                }
            });

            res.on('error', function (e) {
                console.error(e);
            })
        });
    }

    getNewEpisodeNumber (){
        return 'S'+ this.lastEpisode.season +' E'+ (this.lastEpisode.episode+1);
    }

    getNewSeasonNumber () {
        return 'S'+ (this.lastEpisode.season+1) +' E1';
    }

};