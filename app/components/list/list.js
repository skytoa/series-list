let series = require('../../modules/series.js');
let html = require('./html.js');
let cmd = require('child_process');

module.exports = function () {
    document.body.querySelector("main").innerHTML = html.baseHtml();

    series.getSeries().forEach((serie) => {
        //Ajout HTML
        document.body.querySelector("main").querySelector(".list-series").innerHTML += html.serieHtml(serie);

        setTimeout(() => {

            //Mise à jour du status
            updateSerieStatut(serie);

            //Suppression de la série
            document.body.querySelector("main").querySelector("#serie-"+serie.id).querySelector(".btn-delete").onclick = () => {
                serie.deleteFromDatabase();
                document.body.querySelector("main").querySelector("#serie-"+serie.id).innerHTML = "";
            };

            //Passage sur DpStream
            document.body.querySelector("main").querySelector("#serie-"+serie.id).querySelector(".btn-look").onclick = () => {
                url = serie.url;
                if(serie.newEpisode !== null){
                    url.replace('.html');
                    url = url + "/saison-"+ serie.newEpisode.season +"/episode-"+ serie.newEpisode.episode +".html";
                }
                cmd.exec('start '+ url);
            };

            //Vu d'un episode
            document.body.querySelector("main").querySelector("#serie-"+serie.id).querySelector(".btn-looked").onclick = () => {
                lookedNewEpisode(serie);
            };

            //Modification d'une série
            document.body.querySelector("main").querySelector("#serie-"+serie.id).querySelector(".btn-change").onclick = () => {
                require('../../app.js')('change', serie);
            };

        }, 10);

    });
};

let lookedNewEpisode = function (serie) {
    if (! document.querySelector("#serie-"+serie.id).querySelector("img").hasAttribute("hidden")) {return;}
    serie.lastEpisode = serie.newEpisode;
    serie.updateFromDatabase();
    document.querySelector("#serie-"+serie.id).querySelector(".last-episode").textContent = 'Saison '+ serie.lastEpisode.season +' Episode '+ serie.lastEpisode.episode;
    updateSerieStatut(serie);
};

let updateSerieStatut = function (serie) {
    serie.newEpisodeAvailable(() => {
        document.querySelector("#serie-"+serie.id).querySelector("img").removeAttribute("hidden");
        document.querySelector("#serie-"+serie.id).querySelector("span").setAttribute("hidden", true);
    }, (text) => {
        document.querySelector("#serie-"+serie.id).querySelector("img").setAttribute("hidden", true);
        document.querySelector("#serie-"+serie.id).querySelector("span").removeAttribute("hidden");
        document.querySelector("#serie-"+serie.id).querySelector("span").innerText = text;
    }, () => {
        document.querySelector("#serie-"+serie.id).querySelector("img").setAttribute("hidden", true);
        document.querySelector("#serie-"+serie.id).querySelector("span").setAttribute("hidden", true);
    });
};